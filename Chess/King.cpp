#include "King.h"
#include <stdlib.h>

/*
function will move the king of the current player
input: the string of the board and the displacement string (ftom where and where to go), and the flag (reference - to be able to be changed)
output: the string of the board after the move
*/
std::string King::move(std::string piecesStr, std::string displacementStr, int& flag)
{
	int currIndex = 0, destIndex = 0;
	if (isValidMove(piecesStr, displacementStr)) //check if the move is valid (if valid...)
	{
		currIndex = Utils::squareToIndex(displacementStr.substr(0, 2)); //get the curr index by the curr square
		destIndex = Utils::squareToIndex(displacementStr.substr(2, 2)); //get the destination index by the destination square
		piecesStr[currIndex] = EMPTY_SQUARE; //put # in the curr index

		//put in the destination index the letter of the piece
		if (piecesStr[WHO_HAS_THE_TURN_INDEX] == WHITE_TURN)
		{
			piecesStr[destIndex] = eKing - DIFF_BIG_AND_SMALL_CHAR;
			piecesStr[currIndex] = EMPTY_SQUARE;
		}
		else
		{
			piecesStr[destIndex] = eKing;
			piecesStr[currIndex] = EMPTY_SQUARE;
		}
		flag = 0; //change the flag to 0 - valid move
	}
	else
	{
		flag = INVALID_PIECE_MOVE; //flag is 6 - invalid piece move
	}
	return piecesStr;
}


/*
function will check if the move is valid (Can the king move as it is input and whether it does not going on others)
input: the string of the board and the displacement string (from where and where to go) 
output: if the move is valid
*/
bool King::isValidMove(std::string piecesStr, std::string displacementStr)
{
	std::string currSquare = displacementStr.substr(0, 2); //the current square
	std::string destSquare = displacementStr.substr(2, 2); //the destination square

	char currLetterIndex = currSquare[0]; //the letter of the current square
	char currNumIndex = currSquare[1]; //the number of the current square

	char destLetterIndex = destSquare[0]; //the letter of the destination square
	char destNumIndex = destSquare[1]; //the number of the destination square

	//check move  in collum
	if (currLetterIndex == destLetterIndex)
	{
		if (abs((int)currNumIndex - (int)destNumIndex) != 1)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	//check move in row
	else if (currNumIndex == destNumIndex)
	{
		if (abs((int)currLetterIndex - (int)destLetterIndex) != 1)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	//check move in curved line
	else if (abs((int)currNumIndex - (int)destNumIndex) == 1 && (abs((int)currLetterIndex - (int)destLetterIndex) == 1))
	{
		return true;
	}

	return false;
}