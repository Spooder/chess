#pragma once
#include "Utils.h"
#include "Board.h"
#include <iostream> // For std::string
#define EMPTY_SQUARE '#'
#define WHO_HAS_THE_TURN_INDEX 64
#define WHITE_TURN '1'
#define BLACK_TURN '0'
#define DIFF_BIG_AND_SMALL_CHAR 32
#define INVALID_PIECE_MOVE 6

enum ePieces
{
	eNone = '#', eRook = 'r', eKnight = 'n', eKing = 'k', eQueen = 'q', eBishop = 'b', ePawn = 'p'
};

class Piece
{
public:
	//Methods:

	// A pure abstract method responsible for moving a piece on the board
	virtual std::string move(std::string piecesStr, std::string displacementStr, int &flag) = 0;

	// A pure abstract method responsible for checking if a move is valid for a given piece
	virtual bool isValidMove(std::string piecesStr, std::string displacementStr) = 0;

};