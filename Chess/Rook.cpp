#include "Rook.h"
#include <iostream>
#define DIFF_BETWEEN_CHAR_NUM_AND_NUM 48
/*
function will move the rook of the current player
input: the string of the board and the displacement string (ftom where and where to go) , and the flag (reference - to be able to be changed)
output: the string of the board after the move
*/
std::string Rook::move(std::string piecesStr, std::string displacementStr, int& flag)
{
	int currIndex = 0, destIndex = 0;
	if (isValidMove(piecesStr, displacementStr)) //check if the move is valid (if valid...)
	{
		currIndex = Utils::squareToIndex(displacementStr.substr(0, 2)); //get the curr index by the curr square
		destIndex = Utils::squareToIndex(displacementStr.substr(2, 2)); //get the destination index by the destination square
		piecesStr[currIndex] = EMPTY_SQUARE; //put # in the curr index

		//put in the destination index the letter of the piece
		if (piecesStr[WHO_HAS_THE_TURN_INDEX] == WHITE_TURN)
		{
			piecesStr[destIndex] = eRook - DIFF_BIG_AND_SMALL_CHAR;
			piecesStr[currIndex] = EMPTY_SQUARE;
		}
		else
		{
			piecesStr[destIndex] = eRook;
			piecesStr[currIndex] = EMPTY_SQUARE;
		}
		flag = 0; //change the flag to 0 - valid move
	}
	else
	{
		flag = INVALID_PIECE_MOVE; //flag is 6 - invalid piece move
	}
	return piecesStr;
}

/*
function will check if the move is valid (Can the rook move as it is input and whether it does not going on others)
input: the string of the board, the displacement string (ftom where and where to go)
output: if the move is valid
*/
bool Rook::isValidMove(std::string piecesStr, std::string displacementStr)
{
	int i = 0;
	std::string currSquare = displacementStr.substr(0,2); //the current square
	int currSquaeIndexInPiecesStr = Utils::squareToIndex(currSquare); //the index of the current square in the string
	std::string destSquare = displacementStr.substr(2,2); //the destination square
	int destSquaeIndexInPiecesStr = Utils::squareToIndex(destSquare); //the index of the destination square in the string

	char currLetterIndex = currSquare[0]; //the letter of the current square
	char currNumIndex = currSquare[1] - DIFF_BETWEEN_CHAR_NUM_AND_NUM; //the number of the current square

	char destLetterIndex = destSquare[0]; //the letter of the destination square
	char destNumIndex = destSquare[1] - DIFF_BETWEEN_CHAR_NUM_AND_NUM; //the number of the destination square

	//if the move is not by rows or collums - it is invalid
	if (!(destLetterIndex == currLetterIndex || currNumIndex == destNumIndex))
	{
		return false;
	}

	//if the move is in row
	if (destLetterIndex == currLetterIndex)
	{
		std::string stepStr = "";
		//go on the numbers between the current number and destination
		for (i = currNumIndex; (stepStr.compare(destSquare) != 0); i += (currNumIndex < destNumIndex ? 1 : -1))
		{
			char c = '0' + i; //the number of the collum in char
			stepStr = ""; //squares in the way of the displacement
			//push to the string the row and collum
			stepStr.push_back(destLetterIndex);
			stepStr.push_back(c);
			int stepIndex = Utils::squareToIndex(stepStr); //the index of the square in the way
			if (currSquaeIndexInPiecesStr != stepIndex)
			{
				if (destSquaeIndexInPiecesStr != stepIndex && piecesStr[stepIndex] != eNone) //if the piece string in  the index of the square is not empty, its not valid move
				{
					return false;
				}
			}
		}	

	}

	//if the move is in collum
	else if (destNumIndex == currNumIndex)
	{
		char c = destNumIndex + DIFF_BETWEEN_CHAR_NUM_AND_NUM; //the number of the collum in char
		std::string stepStr = "";
		for (char j = currLetterIndex; (stepStr.compare(destSquare) != 0); j += (currLetterIndex < destLetterIndex ? 1 : -1))
		{
			stepStr = ""; //squares in the way of the displacement
			//push to the string the row and collum
			stepStr.push_back(j);
			stepStr.push_back(c);
			int stepIndex = Utils::squareToIndex(stepStr); //the index of the square in the way
			if (currSquaeIndexInPiecesStr != stepIndex)
			{
				if (destSquaeIndexInPiecesStr != stepIndex && piecesStr[stepIndex] != eNone) //if the piece string in  the index of the square is not empty, its not valid move
				{
					return false;
				}
			}
		}
	}

	return true;
}














































