#include "Bishop.h"
#include <stdlib.h>
#include <stdlib.h>  
#include <iostream> 
#include <vector>

/*
function will move the bishop of the current player
input: the string of the board and the displacement string (ftom where and where to go), and the flag (reference - to be able to be changed)
output: the string of the board after the move
*/
std::string Bishop::move(std::string piecesStr, std::string displacementStr, int& flag)
{
	int currIndex = 0, destIndex = 0;
	if (isValidMove(piecesStr, displacementStr)) //check if the move is valid (if valid...)
	{
		currIndex = Utils::squareToIndex(displacementStr.substr(0, 2)); //get the curr index by the curr square
		destIndex = Utils::squareToIndex(displacementStr.substr(2, 2)); //get the destination index by the destination square
		piecesStr[currIndex] = EMPTY_SQUARE; //put # in the curr index

		//put in the destination index the letter of the piece
		if (piecesStr[WHO_HAS_THE_TURN_INDEX] == WHITE_TURN)
		{
			piecesStr[destIndex] = eBishop - DIFF_BIG_AND_SMALL_CHAR;
			piecesStr[currIndex] = EMPTY_SQUARE;
		}
		else
		{
			piecesStr[destIndex] = eBishop;
			piecesStr[currIndex] = EMPTY_SQUARE;
		}
		flag = 0; //change the flag to 0 - valid move
	}
	else
	{
		flag = INVALID_PIECE_MOVE; //flag is 6 - invalid piece move
	}
	return piecesStr;
}


/*
function will check if the move is valid (Can the bishop move as it is input and whether it does not going on others)
input: the string of the board and the displacement string (from where and where to go)
output: if the move is valid
*/
bool Bishop::isValidMove(std::string piecesStr, std::string displacementStr)
{
	std::string currSquare = displacementStr.substr(0, 2); //the current square
	std::string destSquare = displacementStr.substr(2, 2); //the destination square

	char currLetterIndex = currSquare[0]; //the letter of the current square
	char currNumIndex = currSquare[1]; //the number of the current square

	char destLetterIndex = destSquare[0]; //the letter of the destination square
	char destNumIndex = destSquare[1]; //the number of the destination square

	//check if the move is in slant
	if (abs(currLetterIndex - destLetterIndex) == abs(currNumIndex - destNumIndex))
	{
		//if the slant is in one square, it is necessarily valid
		if (abs(currLetterIndex - destLetterIndex) == 1)
		{
			return true;
		}

		//vector of all letters in the way (except the last one)
		std::vector<char> lettersInWay;
		for (char i = currLetterIndex; i != destLetterIndex; i += (currLetterIndex < destLetterIndex ? 1 : -1))
		{
			lettersInWay.push_back(i);
		}

		//vector of all numbers in the way (except the last one)
		std::vector<char> numsInWay;
		for (char i = currNumIndex; i != destNumIndex; i += (currNumIndex < destNumIndex ? 1 : -1))
		{
			numsInWay.push_back(i);
		}
		
		//string of the squares in the way (except the last one)
		std::string squareInWay;
		squareInWay += lettersInWay[1];
		squareInWay += numsInWay[1];
		//check if during the way, the bishop is going on pieces
		for (int i = 1; i < numsInWay.size(); i++)
		{
			if (piecesStr[Utils::squareToIndex(squareInWay)] != eNone)
			{
				return false;
			}
			squareInWay = "";
			squareInWay += lettersInWay[i];
			squareInWay += numsInWay[i];
		}

		return true;
	}

	else
	{
		return false;
	}

}
































