#pragma once
#include "Board.h"

class Game
{
private:
	Board* _board;
public:
	Game(); // C'tor
	~Game(); // D'tor
	void startGame(); // Serves as the game engine. Will run turnes on a loop
};