#pragma once
#include "Board.h"

class Utils
{
public:

	// Methods:
	static void printBoardToConsole( Board& board); // Prints the current gameboard to the console

	static void switchTurns(Board& board); // Switches turns
	static void printTurn(Board& board); // Prints who's turn this is

	// Receives a letter and a number representing a square and returns its index in the board string
	static int squareToIndex(const std::string square); 

	static bool insideBoardRange(std::string displacementStr); // Checks if two given squares are inside the board

	static void printFlagMessage(const int flag); // Receives a flag and prints an appropriate message

	static std::string findKingSquare(const std::string piecesStr, bool findWhiteKing); // Finds the king of given player
	
	static std::string indexToSquare(const int index); // Converts an index in the string to an address of a square
};