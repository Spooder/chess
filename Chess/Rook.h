#pragma once
#include "Piece.h"


class Rook : public Piece
{
public:
	static std::string move(std::string piecesStr, std::string displacementStr, int& flag); // Moves the unit

	static bool isValidMove(std::string piecesStr, std::string displacementStr); // Checks if a given move is valid

};