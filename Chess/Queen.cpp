#include "Queen.h"
#include "Rook.h"
#include "Bishop.h"
#include <iostream>
#define DIFF_BETWEEN_CHAR_NUM_AND_NUM 48
/*
function will move the queen of the current player
input: the string of the board and the displacement string (ftom where and where to go) , and the flag (reference - to be able to be changed)
output: the string of the board after the move
*/
std::string Queen::move(std::string piecesStr, std::string displacementStr, int& flag)
{
	int currIndex = 0, destIndex = 0;
	if (isValidMove(piecesStr, displacementStr)) //check if the move is valid (if valid...)
	{
		currIndex = Utils::squareToIndex(displacementStr.substr(0, 2)); //get the curr index by the curr square
		destIndex = Utils::squareToIndex(displacementStr.substr(2, 2)); //get the destination index by the destination square
		piecesStr[currIndex] = EMPTY_SQUARE; //put # in the curr index

		//put in the destination index the letter of the piece
		if (piecesStr[WHO_HAS_THE_TURN_INDEX] == WHITE_TURN)
		{
			piecesStr[destIndex] = eQueen - DIFF_BIG_AND_SMALL_CHAR;
			piecesStr[currIndex] = EMPTY_SQUARE;
		}
		else
		{
			piecesStr[destIndex] = eQueen;
			piecesStr[currIndex] = EMPTY_SQUARE;
		}
		flag = 0; //change the flag to 0 - valid move
	}
	else
	{
		flag = INVALID_PIECE_MOVE; //flag is 6 - invalid piece move
	}
	return piecesStr;
}

/*
function will check if the move is valid (Can the queen move as it is input and whether it does not going on others)
input: the string of the board, the displacement string (from where and where to go)
output: if the move is valid
*/
bool Queen::isValidMove(std::string piecesStr, std::string displacementStr)
{
	return Rook::isValidMove(piecesStr, displacementStr) || Bishop::isValidMove(piecesStr, displacementStr);
}



