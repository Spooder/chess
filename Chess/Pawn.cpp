#include "Pawn.h"
#include <iostream>
#define DIFF_BETWEEN_CHAR_NUM_AND_NUM 48
#define MIN_ROW_WHITE '7'
#define MIN_ROW_BLACK '2'
/*
function will move the pawn of the current player
input: the string of the board and the displacement string (ftom where and where to go) , and the flag (reference - to be able to be changed)
output: the string of the board after the move
*/
std::string Pawn::move(std::string piecesStr, std::string displacementStr, int& flag)
{
	int currIndex = 0, destIndex = 0;
	if (isValidMove(piecesStr, displacementStr)) //check if the move is valid (if valid...)
	{
		currIndex = Utils::squareToIndex(displacementStr.substr(0, 2)); //get the curr index by the curr square
		destIndex = Utils::squareToIndex(displacementStr.substr(2, 2)); //get the destination index by the destination square
		piecesStr[currIndex] = EMPTY_SQUARE; //put # in the curr index

		//put in the destination index the letter of the piece
		if (piecesStr[WHO_HAS_THE_TURN_INDEX] == WHITE_TURN)
		{
			piecesStr[destIndex] = ePawn - DIFF_BIG_AND_SMALL_CHAR;
			piecesStr[currIndex] = EMPTY_SQUARE;
		}
		else
		{
			piecesStr[destIndex] = ePawn;
			piecesStr[currIndex] = EMPTY_SQUARE;
		}
		flag = 0; //change the flag to 0 - valid move
	}
	else
	{
		flag = INVALID_PIECE_MOVE; //flag is 6 - invalid piece move
	}
	return piecesStr;
}

/*
function will check if the move is valid (Can the pawn move as it is input and whether it does not going on others)
input: the string of the board, the displacement string (ftom where and where to go)
output: if the move is valid
*/
bool Pawn::isValidMove(std::string piecesStr, std::string displacementStr)
{
	std::string currSquare = displacementStr.substr(0, 2); //the current square
	std::string destSquare = displacementStr.substr(2, 2); //the destination square

	char currLetterIndex = currSquare[0]; //the letter of the current square
	char currNumIndex = currSquare[1]; //the number of the current square

	char destLetterIndex = destSquare[0]; //the letter of the destination square
	char destNumIndex = destSquare[1]; //the number of the destination square

	if (currLetterIndex == destLetterIndex) //move must be in the same collum
	{
		if (piecesStr[WHO_HAS_THE_TURN_INDEX] == '1') //if the turn in of white
		{
			if (destNumIndex < currNumIndex) //the move must be forward (up to down the board)
			{
				//if the pawn is in his first row and want to move 2 steps
				if (currNumIndex == MIN_ROW_WHITE && currNumIndex - destNumIndex == 2)
				{
					//check the next square
					std::string nextSquare;
					nextSquare += currLetterIndex;
					nextSquare += currNumIndex - 1;
					std::string nextNextSquare;
					nextNextSquare += currLetterIndex;
					nextNextSquare += currNumIndex - 2;
					if (piecesStr[Utils::squareToIndex(nextSquare)] != eNone || piecesStr[Utils::squareToIndex(nextNextSquare)] != eNone)
					{
						return false;
					}
					else
					{
						return true;
					}
				}
				else if (currNumIndex - destNumIndex == 1) //if the move is 1 step forward - valid if there is no piece
				{
					std::string nextSquare;
					nextSquare += currLetterIndex;
					nextSquare += currNumIndex - 1;
					if (piecesStr[Utils::squareToIndex(nextSquare)] != eNone)
					{
						return false;
					}
					else
					{
						return true;
					}
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			if (destNumIndex > currNumIndex) //the move must be forward (down to up the board)
			{
				//if the pawn is in his first row and want to move 2 steps
				if (currNumIndex == MIN_ROW_BLACK && destNumIndex - currNumIndex == 2)
				{
					//check the next square
					std::string nextSquare;
					nextSquare += currLetterIndex;
					nextSquare += currNumIndex + 1;
					std::string nextNextSquare;
					nextNextSquare += currLetterIndex;
					nextNextSquare += currNumIndex + 2;
					if (piecesStr[Utils::squareToIndex(nextSquare)] != eNone || piecesStr[Utils::squareToIndex(nextNextSquare)] != eNone)
					{
						return false;
					}
					else
					{
						return true;
					}
				}
				else if (destNumIndex - currNumIndex == 1) //if the move is 1 step forward - valid
				{
					std::string nextSquare;
					nextSquare += currLetterIndex;
					nextSquare += currNumIndex + 1;
					if (piecesStr[Utils::squareToIndex(nextSquare)] != eNone)
					{
						return false;
					}
					else
					{
						return true;
					}
				}
			}
			else
			{
				return false;
			}
		}
	}
	else if (abs(currNumIndex - destNumIndex) == 1 && abs(currLetterIndex - destLetterIndex) == 1) //if the pawn tries to eat
	{
		if (piecesStr[WHO_HAS_THE_TURN_INDEX] == '1' && currNumIndex > destNumIndex) //if the turn in of white
		{
			if (piecesStr[Utils::squareToIndex(destSquare)] == eNone)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else if (piecesStr[WHO_HAS_THE_TURN_INDEX] == '0' && destNumIndex > currNumIndex) //if the turn in of white
		{
			if (piecesStr[Utils::squareToIndex(destSquare)] == eNone)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	
	return false;
	

}