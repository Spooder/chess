#include "Utils.h"
#include <iostream>

#define BOARD_SIZE 8
#define TURN_CHAR_INDEX 64

#define SQUARE_LETTER_INDEX 0
#define SQUARE_DIGIT_INDEX 1

#define EXPECTED_LEN 4


/*
Prints the current board to the console
Input:
	Reference to the board object containing the string to be printed
Output:
	None
*/
void Utils::printBoardToConsole(Board& board)
{
	// Prints the board in a square form.
	for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++)
	{
		// Check if a newline needs to be printed
		if (i != 0 && !(i % BOARD_SIZE))
		{
			std::cout << std::endl;
		}

		std::cout << board.getPiecesStr()[i] << " "; // Print single char
	}
	std::cout << std::endl;
}


/*
If the turn char is 1 set it to zero and the other way around
*/
void Utils::switchTurns(Board &board)
{
	std::string piecesStr = board.getPiecesStr();

	// Switch chars
	piecesStr[TURN_CHAR_INDEX] = piecesStr[TURN_CHAR_INDEX] == '0' ? '1' : '0';

	// Update the string
	board.setPiecesStr(piecesStr);
}



/*
Prints [x] player's turn! according to the turn char in board
*/
void Utils::printTurn(Board& board)
{
	std::string turn = "";

	// Find out who's turn this is using the turn char
	turn = board.getPiecesStr()[TURN_CHAR_INDEX] == '1' ? "White" : "Black";

	// Print an appropriate message
	std::cout << turn << " players turn!" << std::endl;
}


/*
Receives a square represented by a letter and a digit and returns it's index in the board string
Input:
	Square to find the index of
Output:
	None
*/
int Utils::squareToIndex(const std::string square)
{
	int index = 0;
	// Seperate the digit and the letter
	char letter = square[SQUARE_LETTER_INDEX];
	char digit = (int)(square[SQUARE_DIGIT_INDEX] - '0');

	// Find the index and return it
	index = BOARD_SIZE * (int)(8 - digit) + letter - 'a';
	return index;
}



/*
Checks if the given action is outside the board range (Only relevant when playing through the console)
Input:
	Action to check
Output:
	True if valid, false otherwise.
*/
bool Utils::insideBoardRange(std::string displacementStr)
{
	// Check if the string contains 4 chars
	if (displacementStr.length() != EXPECTED_LEN)
	{
		return false;
	}
	
	// Check validity of each individual char. 
	for (int i = 0; i < EXPECTED_LEN; i++)
	{
		// Chars at even indexes (0, 2) must be letters
		if (i % 2 == 0 && (int(displacementStr[i]) < int('a') || int(displacementStr[i]) > int('h')))
		{
			return false;
		}
		// Chars at odd indexes (1, 3) must be digits. 
		else if(i % 2 != 0 && (int(displacementStr[i]) < int('1') || int(displacementStr[i]) > int('8')))
		{
			return false;
		}
	}
	return true;
}



/*
Receives a flag number and prints an appropriate message
Input:
	Flag to print
Output:	
	None
*/
void Utils::printFlagMessage(const int flag)
{
	switch (flag)
	{
	case 0:
		return;
		break;
	case 1:
		std::cout << "You perfomed a check!\n";
		break;
	case 2:
		std::cout << "Invalid move! Unit selected does not belong to current player or an empty square was selected\n";
		break;
	case 3:
		std::cout << "Invalid move! Destination contains a unit which belongs to the current player\n";
		break;
	case 4:
		std::cout << "Invalid move! Preforming this action will put your king as risk!\n";
		break;
	case 5:
		std::cout << "Invalid move! Invalid input\n";
		break;
	case 6:
		std::cout << "Invalid move! Selected piece could not perform that action\n";
		break;
	case 7:
		std::cout << "Invalid move! Target square and destination square are the same\n";
		break;
	case 8:
		std::cout << "Performed a check on the enemy player!\n";
		break;
	default:
		break;
	}
}



/*
Return the square in the board where given player's king can be found
Input:
	Which king to find
Output:
	the square where the king can be found
*/
std::string Utils::findKingSquare(const std::string piecesStr, bool findWhiteKing)
{
	// kingChar is either lowercase or uppercases according to which king you are looking for.
	char kingChar = findWhiteKing ? 'K' : 'k';

	int kingIndex = 0;
	for (kingIndex = 0; kingIndex < BOARD_SIZE * BOARD_SIZE; kingIndex++)
	{
		if (piecesStr[kingIndex] == kingChar)
		{
			break;
		}
	}

	// Convert found index to a location of a square
	std::string kingSquare = indexToSquare(kingIndex);

	return kingSquare;
}



/*
Receives an index in the string and converts it to a string representing a square on the board
Input:
	Index to convert
Output:
	Square
*/
std::string Utils::indexToSquare(const int index)
{
	char digit = BOARD_SIZE - (index / BOARD_SIZE) + '0';
	char letter = (index % BOARD_SIZE) + 'a';

	std::string square = "";
	square.push_back(letter);
	square.push_back(digit);

	return square;
}