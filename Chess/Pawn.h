#pragma once
#include "Piece.h"

class Pawn : public Piece
{
public:
	// Move the pawn
	static std::string move(std::string piecesStr, std::string displacementStr, int& flag);

	// Check if the given move is valid for the pawn
	static bool isValidMove(std::string piecesStr, std::string displacementStr);
};