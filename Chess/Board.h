#pragma once
#include <ostream> // For std::string


class Board
{
private:
	std::string _piecesStr; // A string which contains all information about the state of the current game board
	int _flag; // This flag will be changed by action(), and cotains the code of each action (0 - valid move, etc..)
public:
	// Methods:

	std::string attempToPerformAction(std::string piecesStr, std::string displacementStr); // Tries to perfrom a given action if valid.
	std::string action(std::string piecesStr, std::string displacementStr); // Maneger the moves of the pieces

	// Checks if the action is generally valid. Will modify _flag accordingly
	bool checkGeneralFlags(const int targetIndex, const int destIndex); 

	std::string getPiecesStr(); // A getter for Board's _piecesStr value
	void setPiecesStr(const std::string piecesStr); // A setter for Board's _piecesStr value

	int getFlag(); // A getter for Board's _flag variable
	void setFlag(int flag); // A setter for Board's _flag variable

	bool checkSquareAccordingToTurn(const int index, const int flagToCeck); // checks for flags 2 or 3

	bool isKingThreatened(std::string displacementStr, const bool checkCurrentPlayer); //Checks if a king is threatened
};