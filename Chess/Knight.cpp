#include "Knight.h"

#define LETTER_INDEX 0
#define DIGIT_INDEX 1

#define EMPTY_SQUARE '#'
#define WHITE_KNIGHT 'N'
#define BLACK_KNIGHT 'n'

/*
Recieces the current state of the board and an action to perfrom on the knight. 
If the action is valid, performs it, and returns the new state of the board. 
Otherwise, return the board with no changes and raise a flag
Input:
	current state of the board, an action to perfrom and a reference to the flag which will be changed accordingly.
Output:
	The board after the action has been performed.
*/
std::string Knight::move(std::string piecesStr, std::string displacementStr, int& flag)
{
	// Find who's turn this is
	bool isWhiteTurn = piecesStr[WHO_HAS_THE_TURN_INDEX] == '1' ? true : false;

	// Find the location of the knight
	std::string knightSquare = displacementStr.substr(0, 2);
	int knightIndex = Utils::squareToIndex(knightSquare);

	// Find the location of the destination
	std::string destSquare = displacementStr.substr(2, 2);
	int destIndex = Utils::squareToIndex(destSquare);

	// If the action is valid, perform it and raise flag 0
	if (Knight::isValidMove(piecesStr, displacementStr))
	{
		// Make the knights previous poisiton empty
		piecesStr[knightIndex] = EMPTY_SQUARE;

		// Move the knight to it's destination
		piecesStr[destIndex] = isWhiteTurn ? WHITE_KNIGHT : BLACK_KNIGHT;

		// Change the flag to 0 - valid action
		flag = 0;

		// Return the changed board
		return piecesStr;
	}
	// If the action is invalid, raise flag 6 and return the board with no changes
	else
	{
		flag = INVALID_PIECE_MOVE;
		return piecesStr;
	}
}


/*
Recieves the state of the board and an action to perform. Checks if the action is valid for a knight.
Returns true if its valid and false otherwise
Input:
	Current state of the board and the action to perform.
Output:
	True or false depending on whether the action is valid or not.
*/
bool Knight::isValidMove(std::string piecesStr, std::string displacementStr)
{	
	// Find the location of the knight
	std::string knightSquare = displacementStr.substr(0, 2); 

	// Find the location of the destination
	std::string destSquare = displacementStr.substr(2, 2); //the destination square

	// Seperate the knight square into a digit and a letter
	char knightLetter = knightSquare[LETTER_INDEX];
	char knightDigit = knightSquare[DIGIT_INDEX];

	// Seperate the destination square into a digit and a letter
	char destLetter = destSquare[LETTER_INDEX];
	char destDigit = destSquare[DIGIT_INDEX];

	// A knights move will only be valid if the following condition applies:
	if ((abs(knightLetter - destLetter) == 2 && abs(knightDigit - destDigit) == 1 ||
		(abs(knightLetter - destLetter) == 1 && abs(knightDigit - destDigit) == 2)))
	{
		// Valid action - return true
		return true;
	}

	// Invalid action - return false
	return false;
}