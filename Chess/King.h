#pragma once
#include "Piece.h"

class King : public Piece
{
public:
	// Move the king
	static std::string move(std::string piecesStr, std::string displacementStr, int& flag);

	// Check if the given move is valid for the king
	static bool isValidMove(std::string piecesStr, std::string displacementStr);
};