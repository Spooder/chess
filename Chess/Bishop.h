#pragma once
#include "Piece.h"

class Bishop : public Piece
{
public:
	// Move the bishop
	static std::string move(std::string piecesStr, std::string displacementStr, int& flag);

	// Check if the given move is valid for the bishop
	static bool isValidMove(std::string piecesStr, std::string displacementStr);
};