#include "Board.h"
#include "Utils.h"
#include "Piece.h"
#include "Rook.h"
#include "King.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "Pawn.h"
#include <string>
#include <iostream>

#define TARGET_UNIT_START 0 
#define DEST_UNIT_START 2
#define UNIT_LEN 2
#define EMPTY_SQUARE '#'
#define TURN_CHAR_INDEX 64
#define TWO_FLAG 2
#define THREE_FLAG 3
#define WHITE_TURN 1
#define BLACK_TURN 0

#define THREAT_ON_CURRENT_PLAYER_KING true
#define THREAT_ON_ENEMY_PLAYER_KING false

// A setter for Board's _piecesStr value
void Board::setPiecesStr(const std::string piecesStr)
{
	this->_piecesStr = piecesStr;
}

// A getter for Board's _piecesStr value
std::string Board::getPiecesStr()
{
	return this->_piecesStr;
}

int Board::getFlag()
{
	return this->_flag;
}


void Board::setFlag(int flag)
{
	this->_flag = flag;
}

/*
Attempt to perform an action.
First, identify which piece the action is performed on, and do not perfrom if invalid.
Then, attempt to move the piece if the action is valid according to said piece's movement.
Input:
	Action to attempt to perform
Output:
	piecesStr after the action
*/
std::string Board::attempToPerformAction(std::string piecesStr, const std::string displacementStr)
{
	std::string targetSquare = "", destSquare = "";
	int targetIndex = 0; int destIndex = 0;
	char targetUnit = '0';

	/* divide the action (displacementStr) into two parts:
	- target: the piece to attemp to move
	- dest: where to move it to */
	targetSquare = displacementStr.substr(TARGET_UNIT_START, UNIT_LEN);
	destSquare = displacementStr.substr(DEST_UNIT_START, UNIT_LEN);

	targetIndex = Utils::squareToIndex(targetSquare);
	destIndex = Utils::squareToIndex(destSquare);

	// Use the target index to find the target unit.
	targetUnit = piecesStr[targetIndex];
	
	// If the action is invalid, return piecesStr without changes     
	if (!checkGeneralFlags(targetIndex, destIndex) || piecesStr == this->action(piecesStr, displacementStr)
		|| isKingThreatened(displacementStr, THREAT_ON_CURRENT_PLAYER_KING)) 
	{
		return piecesStr;
	}
	else
	{
		// Check if the last action threatens the ENEMY king (this will raise flag 1). Then print the flag message.
		if (isKingThreatened(displacementStr, THREAT_ON_ENEMY_PLAYER_KING))
		{
			std::string result = action(piecesStr, displacementStr);
			this->_flag = 1;
			return result;
		}

		return action(piecesStr, displacementStr);
	}
}

/*
function will manage the moves of the pieces
input: the pieces letter, the string of the board and the displacement string
output: the string of the board after the move
*/
std::string Board::action(std::string piecesStr, std::string displacementStr)
{
	// Extract the target unit from displacementStr
	std::string targetSquare = displacementStr.substr(TARGET_UNIT_START, UNIT_LEN);
	int targetIndex = Utils::squareToIndex(targetSquare);
	char targetUnit = piecesStr[targetIndex];

	// Call the appropriate move method accodring to the target unit in question
	switch (tolower(targetUnit))
	{
	case eRook:
		piecesStr = Rook::move(piecesStr, displacementStr, this->_flag);
		break;
	case eKing:
		piecesStr = King::move(piecesStr, displacementStr, this->_flag);
		break;
	case eKnight:
		piecesStr = Knight::move(piecesStr, displacementStr, this->_flag);
		break;
	case eBishop:
		piecesStr = Bishop::move(piecesStr, displacementStr, this->_flag);
		break;
	case ePawn:
		piecesStr = Pawn::move(piecesStr, displacementStr, this->_flag);
		break;
	case eQueen:
		piecesStr = Queen::move(piecesStr, displacementStr, this->_flag);
	default:
		break;
	}
	
	return piecesStr;
}


/*
This funtion can check for either flag 2 or 3 - It receives an index and the flag to check for.
When checking for flag 2, will raise the flag if the given index (targetIndex) belogns to the enemy player
When checking for flag 3, will raise the flag if the given index (destIndex) belogns to the current player
Input:
	index to check on the board and the flag to check for
Output:
	False if the action is invalid
*/
bool Board::checkSquareAccordingToTurn(const int index, const int flagToCeck)
{
	int whoHasTheTurn = this->_piecesStr[TURN_CHAR_INDEX] - '0'; // Check which player has the turn
	
	// If checking for flag 3, swap whoHasTheTurn, to check if the target square contains a unit that belong to
	// the enemey player
	if (flagToCeck == THREE_FLAG)
	{
		whoHasTheTurn = whoHasTheTurn == BLACK_TURN ? WHITE_TURN : BLACK_TURN;
	}
	if (whoHasTheTurn) // White player turn
	{
		if (this->_piecesStr[index] >= 'a' && this->_piecesStr[index] <= 'z') // If the piece is black
		{
			this->_flag = flagToCeck; // Change the flag accordingly
			return false;
		}
	}
	else // Black player turn
	{
		if (this->_piecesStr[index] >= 'A' && this->_piecesStr[index] <= 'Z') // If the piece is white
		{
			this->_flag = flagToCeck; // Change the flag accordingly
			return false;
		}
	}

	return true;	
}



/*
Check if an action is generally valid 
An action will be invalid if:
	- Target unit does not belong to the current player (Either empty or an enemy unit) [flag = 2]
	- Destination belongs to current player [flag = 3]
	- target and destination are the same [flag = 7]
	in that case, return false
*/
bool Board::checkGeneralFlags(const int targetIndex, const int destIndex)
{
	// Check for flag 7 = Target and destination are the same
	if (targetIndex == destIndex)
	{
		this->_flag = 7;
		return false;
	}

	//Check for  flag 2
	if (this->_piecesStr[targetIndex] == eNone || !checkSquareAccordingToTurn(targetIndex, TWO_FLAG))
	{
		this->_flag = 2;
		return false;
	}

	//Check for flag 3
	if (!checkSquareAccordingToTurn(destIndex, THREE_FLAG))
	{
		this->_flag = 3;
		return false;
	}

	return true;
}




/*
Checks if a certain given action puts either king under threat.
If checkCurrentPlayer is true, the function will check whether the player who perfromed the move is under threat.
When false, it will check whether the move puts the enemey player under threat
Input:
	an action to check if it puts a king under threat and which king to check.
Output:
	True if the king is underthreat, false otherwise.
*/
bool Board::isKingThreatened(std::string displacementStr, const bool checkCurrentPlayer)
{
	// Perform the action and keep the result locally
	std::string boardStr = this->action(this->_piecesStr, displacementStr);
	
	// Use given information to determine which king to check
	bool isWhitePlayerTurn = this->_piecesStr[TURN_CHAR_INDEX] == '1' ? true : false;
	isWhitePlayerTurn = checkCurrentPlayer ? isWhitePlayerTurn : !isWhitePlayerTurn;
	
	// Locate the king to check if its under threat
	std::string kingSquare = Utils::findKingSquare(boardStr, isWhitePlayerTurn);
	
	// When checking the current player, the char representing who's turn this is must be swapped, since it will
	// be used inside the functions checking for threats on the king.
	if (checkCurrentPlayer)
	{
		boardStr[TURN_CHAR_INDEX] = boardStr[TURN_CHAR_INDEX] == '1' ? '0' : '1';
	}

	// Loop over the entire board. Locate any enemey pieces that may threat the king, and check,
	// based on their current position and the kings current position wheteher they threaten him or nor.
	for (int i = 0; i < TURN_CHAR_INDEX; i++)
	{
		// Check pieces that belong to the enemy player
		if ((isupper(boardStr[i]) && !isWhitePlayerTurn) || (islower(boardStr[i]) && isWhitePlayerTurn))
		{
			// Use these to create a string representing the action to perform
			std::string actionStr = Utils::indexToSquare(i) + kingSquare;

			// If the board changed after performing the action (meaning the action was valid) the king is threatened
			std::string temp = this->action(boardStr, actionStr);
			if (boardStr != temp)
			{
				// Change the flags accordingly
				this->_flag = checkCurrentPlayer ? 4 : 1;
				return true;
			}
		}
	}
	return false;
}