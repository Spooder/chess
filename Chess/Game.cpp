#include "Game.h"
#include "Pipe.h"
#include "Utils.h"
#include "Pipe.h"
#include <iostream>
#include <thread>

					// "RNBKQBNRPPPPPPPP################################pppppppprnbkqbnr1"
#define NEW_GAME_STR "RNBKQBNRPPPPPPPP################################pppppppprnbkqbnr1"

// Graphics expects 0 to be white turn and 1 to be black turn. 
//To overcome that, the turns must be switched.
#define GRAPHICS_NEW_GAME_STR "RNBKQBNRPPPPPPPP################################pppppppprnbkqbnr0"

#define FLAG_FIVE 5
/*
This method serves as the game engine. It will runs turns in a loop until no further action can be performed
The game engine will:
- Be responsible for switching the players' turns
- Communicate with the graphic engine using a pipe
- Attempt to perform actions on the board received via the pipe
Input:
	None
Output:
	None
*/
void Game::startGame()
{
	srand(time_t(NULL));
	Pipe p;
	bool isConnect = p.connect();
	string ans;

	// Attempt to connect to graphics
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return;
		}
	}


	// Once connected, initialize the board in the starting state
	this->_board->setPiecesStr(NEW_GAME_STR);
	
	// Send the starting board through the pipe
	char msgToGraphics[1024];
	strcpy_s(msgToGraphics, GRAPHICS_NEW_GAME_STR);
	p.sendMessageToGraphics(msgToGraphics);

	std::cout << "Started a new game!" << std::endl;
	std::string msgFromGraphics = "";

	while (msgFromGraphics != "quit")
	{
		// Print who's turn this is
		Utils::printTurn(*(this->_board));

		Utils::printBoardToConsole(*(this->_board));

		std::string nextAction = "";
		std::string result = "";

		// Try to perform an action
		do
		{
			// Print a flag if one is raised
			Utils::printFlagMessage(this->_board->getFlag());

			// Reset current flag
			this->_board->setFlag(0);

			// Check if the input is valid. Loop until it is valid.
			checkInput:
				// Playing through the graphics:
				msgFromGraphics = p.getMessageFromGraphics();
				nextAction = msgFromGraphics;
				
				 // Playing through the console:
				//std::cout << "Enter your next action:\n";
				//std::cin >> nextAction;
				if (!Utils::insideBoardRange(nextAction))
				{
					Utils::printFlagMessage(FLAG_FIVE);
					goto checkInput; // Loop until the input is valid
				}

				result = this->_board->attempToPerformAction(this->_board->getPiecesStr(), nextAction); // Try to perfrom the action and save the result
			
				// Send the flag raised as a result of the last action to the graphics
				strcpy_s(msgToGraphics, std::to_string(this->_board->getFlag()).c_str());
				p.sendMessageToGraphics(msgToGraphics);

		} while (this->_board->getPiecesStr() == result); // Repeat until the board has changed.
		
		// The action was valid, update the board.
		this->_board->setPiecesStr(result);

		// Turn over. Now switch
		Utils::switchTurns(*(this->_board));
	}
}





// C'tor
Game::Game()
{
	this->_board = new Board();
}


// D'tor
Game::~Game()
{
	delete this->_board;
}




