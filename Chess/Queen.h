#pragma once
#include "Piece.h"

class Queen : public Piece
{
public:
	// Move the queen
	static std::string move(std::string piecesStr, std::string displacementStr, int& flag);

	// Check if the given move is valid for the queen
	static bool isValidMove(std::string piecesStr, std::string displacementStr);
};